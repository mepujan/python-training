class Employee:
    def __init__(self,name,address,salary):
        self.name=name
        self.address=address
        self.salary=salary
    def calculate_tax(self):
            return self.salary * 0.01


class Teacher(Employee):
    def __init__(self, name, address,fulltime, subject, salary):
        self.fulltime = fulltime
        self.subject = subject
        self.salary = salary
        super().__init__(name,address,salary)

    def take_class(self):
        print('Class taken')


class Helper(Employee):
    def __init__(self, name, address, salary):
        super().__init__(name,address,salary)

    def manage_parking(self):
        print('Parking managed')

    def clean_desk(self):
        print('Desk cleaned')


class Manager(Employee):
    def __init__(self, name, address,salary, department):
        self.department = department
        super().__init__(name,address,salary)

    def assign_task(self):
        print('Task Assigned')


sample_teacher = Teacher('Pujan', 'Dhapakhel', True, 'python', 200000)
sample_helper = Helper('Mukesh', 'harisiddhi', 150000)
sample_manager = Manager('Santosh', 'Tinkune', 750000, 'Science Department')
print('Taskable Amount of teacher=', sample_teacher.calculate_tax())
sample_teacher.take_class()
print('Taskable Amount of helper=', sample_helper.calculate_tax())
sample_helper.manage_parking()
sample_helper.clean_desk()
print('Taskable Amount of manager=', sample_manager.calculate_tax())
sample_manager.assign_task()
