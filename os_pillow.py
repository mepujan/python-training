import os
from PIL import Image,ImageEnhance
SRC_DIR=r'/home/pujan/Desktop/photos'
DES_DIR=r'/home/pujan/Desktop/brighter_images'

for image in os.listdir(SRC_DIR):
    #print(image)
    full_path=os.path.join(SRC_DIR,image)
    save_full_path=os.path.join(DES_DIR,image)
    im=Image.open(full_path)
    # bw=im.convert('L')
    # bw.show()
    enh=ImageEnhance.Brightness(im)
    enh.enhance(1.2).save(save_full_path)
