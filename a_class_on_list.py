sample_list=['ram','shyam','hari','john','john'] #homogenous list(same datatype)
print(len(sample_list))
for element in sample_list:
    print(element)
print(sample_list[-2])
print(sample_list[0])
print(sample_list[1:3])
print(sample_list[::-1])


sample_list1=['ram','shyam',1,12.22] #heterogenous list
for items in sample_list1:
    print(items)
print(sample_list1[-2])

del sample_list1[0]
print(sample_list1)
sample_list1.append('ram')
print(sample_list1)
sample_list[0]='pujan'
print(sample_list)
print(sample_list.count('john'))
sample_list.insert(3,'rajesh')
print(sample_list)
print(sample_list.index('hari'))
last_name=sample_list.pop()
print(sample_list)
print(last_name)
first_name=sample_list.pop(0)
print(first_name)
