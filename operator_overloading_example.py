class Height:  # 12 inch==1ft
    def __init__(self, ft, inch):
        self.ft = ft
        self.inch = inch

    def __str__(self):
        return '%d ft %d inch' % (self.ft, self.inch)

    def __sub__(self, other):
        ft = self.ft - other.ft
        inch = self.inch - other.inch

        return Height(ft, inch)


sample_height1 = Height(7, 5)
print(sample_height1)
sample_height2 = Height(6, 4)
print(sample_height2)
sample_height3 = sample_height1 - sample_height2
print(sample_height3)
