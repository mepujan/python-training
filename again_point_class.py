# constructor in OOP
import math


class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __str__(self):  # calls when class object is needed in string
        return 'point(%d ,%d)' % (self.x, self.y)


a = Point(3, 4)
b = Point(5, 7)

distance = math.sqrt((a.x - b.x) ** 2 + (a.y - b.y) ** 2)
print('Distance between two points=', distance)
print(a)
