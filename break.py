for i in range(100):
    print("hello world", i)
    if i == 50:
        break


def prime_or_composite(num):
    prime = True
    for i in range(2, num):
        if num % i == 0:
            prime = False
            break
    return prime


num = int(input('Enter the number:'))
print(prime_or_composite(num))

for i in range(1, 11):
    print(2 ** i)

for i in range(1, 11):
    print(3 ** i)

for i in range(2, 10):
    if i % 2 == 0:
        print(i ** 2)
    else:
        print(i ** 3)

n=1
for i in range(5):
    print(n)
    n=n*10+1

n=1
for i in range(5):
    print(n**2)
    n=n*10+1

n=4
for i in range(5):
    print(n)
    n=n*10+4


