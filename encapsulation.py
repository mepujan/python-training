class Customer:
    def __init__(self, name, balance, address, account_type):
        self.name = name
        self.__balance = balance
        self.address = address
        self.account_type = account_type

    def deposite_amt(self, amount):
        self.__balance += amount

    def withdraw(self, amount):
        if self.__balance >= amount:
            self.__balance -= amount
        else:
            print('Insufficent balance')

    def print_balance(self):
        print("Balance=",self.__balance)


pujan = Customer('Pujan Gautam', 5000000, 'Dhapakhel', 'Saving')
pujan.print_balance()
pujan.deposite_amt(20000)
pujan.print_balance()
pujan.withdraw(20000)
pujan.print_balance()