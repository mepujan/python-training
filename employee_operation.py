class Employee:
    name=None
    gender=None
    address=None
    marital_status=None
    yearly_income=None
    def __init__(self,name,gender,address,marital_status,yearly_income):
        self.name=name
        self.gender=gender
        self.address=address
        self.marital_status=marital_status
        self.yearly_income=yearly_income

    def calculate_tax(self):
        if self.marital_status==True and self.yearly_income>450000:
            taxable_amount=self.yearly_income-450000
            return taxable_amount*0.15

        elif self.marital_status==False and self.yearly_income>350000:
            taxable_amount = self.yearly_income - 350000
            return taxable_amount*0.15

        else:
            return self.yearly_income*0.01
pujan=Employee('pujan','M','Dhapakhel',False,55000)
print('Taxable amount=',pujan.calculate_tax())

