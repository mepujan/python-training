import math
class Rectangel:
    def __init__(self,l,b):
        self.l=l
        self.b=b
    def area(self):
        return self.l*self.b
    def perimeter(self):
        return 2*(self.l+self.b)
    def is_square(self):
        if self.l==self.b:
            return 'square'
    def diagonal(self):
        return math.sqrt(self.l**2+self.b**2)
sample_rect=Rectangel(3,4)
print('Area=',sample_rect.area())
print('Perimeter=',sample_rect.perimeter())
print('Rectangle is  a',sample_rect.is_square())
print('Diagonal=',sample_rect.diagonal())

class Triangle:
    def __init__(self,side1,side2,side3):
        self.side1=side1
        self.side2=side2
        self.side3=side3

    def area(self):
        s=(self.side1+self.side2+self.side3)/2
        return math.sqrt(s*(s-self.side1)*(s-self.side2)*(s-self.side3))
    def isosceles_triangle(self):
        if self.side1==self.side2 or self.side1==self.side3 or self.side2==self.side3:
            return 'isosceles triangle'
    def equilateral_triangle(self):
        if self.side1==self.side2==self.side3:
            return 'equilateral triangle'
sample_triangle=Triangle(2,2,2)
print('Area of triangle=',sample_triangle.area())
print('Triangle is a',sample_triangle.isosceles_triangle())
print('Triangle is',sample_triangle.equilateral_triangle())

class Circle:
    def __init__(self,radius):
        self.radius=radius
    def area(self):
        return math.pi*self.radius**2
    def circumference(self):
        return 2*math.pi*self.radius
    def diagonal(self):
        return 2*self.radius

sample_circle=Circle(7)
print('Area of circle=',sample_circle.area())
print('Circumference of circle=',sample_circle.circumference())
print('Diagonal of circle=',sample_circle.diagonal())


class Cuboid:
    def __init__(self,l,b,h):
        self.length=l
        self.breadth=b
        self.height=h
    def volume(self):
        return self.length*self.breadth*self.height


cuboid_sample=Cuboid(2,3,4)
print('Volume of cuboid=',cuboid_sample.volume())




