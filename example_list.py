#example 1
marks=[55,70,85,92,80]
total=0
for mark in marks:
    total+=mark
print('The total marks of student=',total)
percentage=round(total/5,2)
print('Precentage obtained=',percentage)
if percentage>=90:
    print('A+')
elif percentage>=80:
    print('A')
elif percentage>=70:
    print('B+')
elif percentage>=60:
    print('B')
else:
    print('calculate yourself')

#example 2

names=['sabitra bhusal','santosh khanal','sobika kadel','lana rose','pujan gautam']
fixed_name=[]
for name in names:
    fixed_name.append(name.title())
print(fixed_name)

fullname=['sabitra bhusal','santosh khanal','sobika kadel','lana rose','pujan gautam']
surnames=[]
firstname=[]
for surname in fullname:
    parts=surname.split(' ')
    surnames.append(parts[-1])
    firstname.append(parts[0])
print('surnames=',surnames)
print('first name=',firstname)


#example 3
names=['ram kumar sharma','pujan gautam','santosh bahadur khanal','gopal tiwari']
with_middle_name=[]
without_middle_name=[]
for name in names:
    parts=name.split(' ')
    if len(parts)==3:
        with_middle_name.append(name)
    else:
        without_middle_name.append(name)
print('name with middle name=',with_middle_name)
print('name without middle name=',without_middle_name)


names=['ram kumar sharma','pujan gautam','santosh bahadur khanal','gopal tiwari']
reverse_name=[]

for name in names:
    parts=name.split(' ')
    reverse_name.append(' '.join(parts[::-1]))
print(reverse_name)







