class Time:
    def __init__(self, hour, m, sec):
        self.hour = hour
        self.min = m
        self.sec = sec

    def __str__(self):
        return '%d hr: %d min: %d sec' % (self.hour, self.min, self.sec)

    def __add__(self, other):
        sec = self.sec + other.sec
        if sec >= 60:
            m = self.min + other.min + 1
            sec = sec - 60
        else:
            m = self.min + other.min
        if m >= 60:
            hour = self.hour + other.hour + 1
            m = m - 60
        else:
            hour = self.hour + other.hour
        return Time(hour, m, sec)


a = Time(10, 35, 40)
b = Time(2, 30, 40)
print(a)
print(b)
c = a + b
print(c)
