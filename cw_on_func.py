import math


def calculate_area_circle(r):
    return math.pi * r ** 2


def centigrate_frah(c):
    return c * (9 / 5) + 32


def calculate_distance_between(x1, x2, y1, y2):
    return math.sqrt((x1 - x2) ** 2 + (y1 - y2) ** 2)


def calculate_volatge(i, r):
    return i * r


def area_of_triangle(a, b, c):
    s = (a + b + c) / 2
    return math.sqrt(s * (s - a) * (s - b) * (s - c))


print('area of circle=', calculate_area_circle(6))
print('frahenite=', centigrate_frah(37))
print('Distance between two points:', calculate_distance_between(4, 5, 6, 7))
print('voltage=', calculate_volatge(12, 8))
print('area of triangle=', area_of_triangle(4, 5, 6))


def day_name(day):
    if day == 1:
        return 'sunday'
    elif day == 2:
        return 'monday'
    elif day == 3:
        return 'tuesday'
    elif day == 4:
        return 'wednesday'
    elif day == 5:
        return 'thrusday'
    elif day == 6:
        return 'friday'
    elif day == 7:
        return 'saturday'
    else:
        return 'try again'


num = int(input('Enter the number:'))
print(day_name(num))


def is_right_angle(a, b, c):
    h = max(a, b, c)
    if h ** 2 == (a ** 2 + b ** 2 + c ** 2) / 2:
        return 'right angle triangle'
    else:
        return 'not right angle traingle'


print(is_right_angle(3, 4, 5))


def isoscelos_triangle(a, b, c):
    if a == b or b == c or a == c:
        return True
    else:
        return False


if isoscelos_triangle(4, 4, 6):
    print('Isoscelos triangle')
else:
    print('not isoscelos')



