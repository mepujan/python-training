#for loop
for i in range(10):
    print(i)

for j in range(1,15):
    print(j)

for k in range(1,15,2):
    print(k)

for l in range(1,101):
    print(l)

for m in range(2,100,2):
    print(m)

for n in range(1,101,2):
    print(n)

for o in range(5,101,5):
    print(o)