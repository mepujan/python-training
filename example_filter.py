names = ['pujan gautam', 'anuja gautam', 'santosh khanal', 'gopal tiwari', 'mukesh maharzan', 'sabitra gautam',
         'sobika gautam', 'nitesh basnet', 'lana gautam', 'paras khadka', 'sachin bhattarai']


def parts(name):
    part = name.split(' ')
    if part[-1] == 'gautam':
        return part


filter_name = filter(parts, names)
print(list(filter_name))

sample_list = [2, 4, 5, 8, 10, 13, 15, 50, 42]
div_by_5 = filter(lambda num: num % 5 == 0, sample_list)
print(list(div_by_5))


names_list=['Ram Pd Sharma','Hari Bhusal','Soman Bd Yadav','Josh Taylor','Roshan K Rijal']
name_with_out_middle_name=filter(lambda name:len(name.split(' '))==2,names_list)
print(list(name_with_out_middle_name))
name_with_middle_name=filter(lambda name:len(name.split(' '))==3,names_list)
print(list(name_with_middle_name))

import math
numbers=[2,3,4,5,5,6,9,25,35,36,45,64,88,47,49]
perfect_square=filter(lambda num:math.sqrt(num)==int(math.sqrt(num)),numbers)
print(list(perfect_square))


multiple_3=filter(lambda num:num%3==0,numbers)
print(list(multiple_3))


location=['kathmandu,NP','Lumbini,NP','Goa,IND','New York,USA','Texas,USA','Pokhara,NP']
nepal_place=filter(lambda place:place.split(',')[-1]=='NP',location)
print(list(nepal_place))


name_list=['mohanmad ali khan','salman ali khan','shaid afradi','pujan gautam','ria','priya','santosh bahadur khanal']
long_name=filter(lambda name:len(name)>=13,name_list)
print(list(long_name))

#numbers_list=[1,2,4,77,5,54,45,87,645,5435,54,153,370, 371, 407, 1634,100,101]
#armstrong_num=filter(lambda num:len(num)

num_list=[0,-5,10,5,-6,7,5,-10,15,-2]
negative_num=filter(lambda num:num<0,num_list)
print(list(negative_num))


#Runs in python 2 only
# data=[2,5,6,7,8]
# r=reduce(lambda a,b:a+b,data)
# print('r is ',r)




