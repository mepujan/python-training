students=[
    {'name':'pujan','email':'pujan@gmail.com','percent':95},
    {'name':'santosh','email':'santosh@yahoo.com','percent':55},
    {'name':'gourav','email':'gourav@outlook.com','percent': 25},
    {'name':'gopal','email':'gopal@gmail.com','percent': 35},
    {'name': 'muks','email':'muks@gmail.com', 'percent': 15}
]
fail_count=0
gmail_count=0
for student in students:
    email_provider=student['email'].split('@')
    if email_provider[-1]=='gmail.com':
        gmail_count+=1
    if student['percent'] < 40:
        fail_count+=1
print('Number of failed students=',fail_count)
print('Number of gmail used=',gmail_count)


