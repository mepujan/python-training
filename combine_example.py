import math

data = [4, 10, 14, 13, 14, 14, 14, 9]
total = 0
mean = sum(data) / len(data)
for item in data:
    total += (item - mean) ** 2
std = math.sqrt(total / len(data))
print('mean=', mean)
print('standard deviation=', std)
data.sort()
print(data)
if len(data) % 2 == 0:
    median = len(data) // 2
else:
    median = (len(data) + 1) // 2
print(median)
print('median=', data[median] - 1)

a = [2, 3, 4, 5]
b = []
for item in a:
    b.append(item ** 2)
print(b)


def check_even_odd(num):
    if num % 2 == 0:
        return True
    else:
        return False


print(check_even_odd(5))

check_even_odd1 = lambda n: n % 2 == 0
print(check_even_odd1(5))

my_sum = lambda a, b: a + b
print(my_sum(10, 20))

# sn name  dob address totalmarks gender

my_diff = lambda a, b: a - b
print(my_diff(10, 5))

rev = lambda string: string[::-1]
print(rev('pujan'))

capital = lambda s: s.capitalize()
print(capital('pujan'))

palindrome = lambda str: str == str[::-1]
print(palindrome('aba'))

hypotenous = lambda p, b: math.sqrt(p ** 2 + b ** 2)
print('hypotenous=', hypotenous(3, 4))

mean = lambda sample_list: sum(sample_list) / len(sample_list)
print('mean=', mean([4, 5, 6, 5]))

names = ['john', 'josh', 'dave']
reverse_names = []
for name in names:
    reverse_names.append(name[::-1])
print(reverse_names)

# new way
new_way = map(lambda s: s[::-1], names)
print(list(new_way))

numbers = [2, 3, 4, 5]
square = map(lambda num: num ** 2, numbers)
print(list(square))

male_name = ['Ram', 'Shyam', 'John']
updated_name = map(lambda name: 'Mr.' + name, male_name)
print(list(updated_name))

female_name = ['Riya', 'Priya', 'sabitra']
updated_female_name = map(lambda name: 'Ms.' + name, female_name)
print(list(updated_female_name))
total_marks = [750, 659, 560, 450, 720, 700]
percentage = map(lambda mark: mark / 8, total_marks)
#print(list(percentage))

data=list(percentage)
def grade(percent):
    if percent >= 90:
        return 'A+'
    elif percent >= 80:
        return 'A'
    elif percent >= 70:
        return 'B+'
    else:
        return 'C'


std_grade = map(grade, data)
print(list(std_grade))

num = [2, 3, 4, 5]
new_num = map(lambda n: 1 / n, num)
print(list(new_num))


#filter
new_numbers=[2,3,4,5,6,7,8]
filter_num=filter(lambda n:n%2==0,new_numbers)
print(list(filter_num))


