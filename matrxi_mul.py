matrix_A = [[1, 2, 3], [3, 4, 5], [6, 7, 8]]
matrix_B = [[1, 2, 3], [3, 4, 5],[6,7,8]]
result = [[0, 0, 0], [0, 0, 0], [0, 0, 0]]
if len(matrix_A) == len(matrix_B):
    for i in range(len(matrix_A)):
        for j in range(len(matrix_B)):
            for k in range(len(matrix_B)):
                result[i][j] += matrix_A[i][k] * matrix_B[k][j]
    for multiply in result:
        print(multiply)
else:
    print('matrix cannot be multiplied')


